import React from 'react'
import Images from './../tech1.webp'

const portfolio = () => {  
    return (
        <section className="page-section bg-light" id="portfolio">
            <div className="container-fluid">
                <div className="row">
                    <div className="col-lg-4 col-sm-6">
                        <div className="portfolio-item">
                            <a className="portfolio-link" data-toggle="modal" href="#portfolioModal1">
                                <div className="portfolio-hover">
                                    <div className="portfolio-hover-content">
                                            <div className="text-muted text-white">Category</div>
                                            <div className="text-white text-uppercase">Tech & Gadgets</div>
                                    </div>
                                </div>
                                <img className="img-fluid" src={Images}  />
                            </a>
                            
                        </div>
                    </div>
                    <div className="col-lg-4 col-sm-6">
                        <div className="portfolio-item">
                            <a className="portfolio-link" data-toggle="modal" href="#portfolioModal2">
                                <div className="portfolio-hover">
                                    <div className="portfolio-hover-content">
                                            <div className="text-muted text-white">CATEGORY</div>
                                            <div className="text-white text-uppercase">Wearables</div>
                                        </div>
                                </div>
                                <img className="img-fluid" src={Images}  />
                            </a>
                        </div>
                    </div>
                    <div className="col-lg-4 col-sm-6">
                        <div className="portfolio-item">
                            <a className="portfolio-link" data-toggle="modal" href="#portfolioModal3">
                                <div className="portfolio-hover">
                                    <div className="portfolio-hover-content">
                                        <div className="text-muted text-white">Category</div>
                                        <div className="text-white text-uppercase">Photography</div>
                                    </div>
                                </div>
                                <img className="img-fluid" src={Images}  />
                            </a>
                        </div>
                    </div>
                    <div className="col-lg-4 col-sm-6 mb-lg-0">
                        <div className="portfolio-item">
                            <a className="portfolio-link" data-toggle="modal" href="#portfolioModal4">
                                <div className="portfolio-hover">
                                    <div className="portfolio-hover-content">
                                        <div className="text-muted text-white">Category</div>
                                        <div className="text-white text-uppercase">Kitchen</div>
                                    </div>
                                </div>
                                <img className="img-fluid" src={Images}  />
                            </a>
                        </div>
                    </div>
                    <div className="col-lg-4 col-sm-6 mb-sm-0">
                        <div className="portfolio-item">
                            <a className="portfolio-link" data-toggle="modal" href="#portfolioModal5">
                                <div className="portfolio-hover">
                                    <div className="portfolio-hover-content">
                                        <div className="text-muted text-white">Category</div>
                                        <div className="text-white text-uppercase">Scientific</div>
                                    </div>
                                </div>
                                <img className="img-fluid" src={Images}  />
                            </a>
                        </div>
                    </div>
                    <div className="col-lg-4 col-sm-6">
                        <div className="portfolio-item">
                            <a className="portfolio-link" data-toggle="modal" href="#portfolioModal6">
                                <div className="portfolio-hover">
                                    <div className="portfolio-hover-content">
                                        <div className="text-muted text-white">Category</div>
                                        <div className="text-white text-uppercase">Transportation</div>
                                    </div>
                                </div>
                                <img className="img-fluid" src={Images}  />
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    )
}

export default portfolio;
