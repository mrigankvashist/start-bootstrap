<div className="App">
      <nav className="navbar navbar-light navbar-expand-lg fixed-top" id="mainNav" style={{ backgroundColor: "rgb(230, 230, 230)" }}>
        <div className="container-fluid"><a className="navbar-brand js-scroll-trigger" href="#page-top" style={{ color: "#ff9933", fontSize: "35px" }}><img src="./../img/fx.jpg" style={{ height: "50px" }} /></a><button data-toggle="collapse" data-target="#navbarResponsive" className="navbar-toggler navbar-toggler-right collapsed" type="button" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation"><i className="fa fa-align-justify"></i></button>
          <div className="collapse navbar-collapse" id="navbarResponsive"><input type="search" style={{ width: "164px", background: "rgba(26, 26, 26, 0.18)", borderWidth: "0px", borderRadius: "18px", padding: "3px", marginRight: "6px", textIndent: "30%", color: "rgb(255, 255, 255)", marginLeft: "39px" }} placeholder="Search" /><i className="fa fa-search" style={{ fontSize: "26px", color: "var(--blue)" }}></i>
            <ul className="nav navbar-nav ml-auto">


              <li className="nav-item"><a className="nav-link js-scroll-trigger" href="./ref/about us.html" style={{ color: "black" }}>ABOUT US</a></li>
              <li className="nav-item"></li>
              <li className="nav-item"><a className="nav-link js-scroll-trigger" href="#portfolio" style={{ color: "black" }}>HOW IT WORKS</a></li>
              <li className="nav-item"><a className="nav-link js-scroll-trigger" href="https://startups.fundingx.in" style={{ color: "black" }}>HEAD-START YOUR ENTERPRISE</a></li>
              <li className="nav-item"></li>
            </ul>
          </div>
        </div>
      </nav>
      <header>
        <video id="bgvideo" playsInline="playsinline" autoPlay="autoplay" muted="muted" loop="loop">
          <source src="./sample.mp4" type="video/mp4" />
        </video>
        <div className="container h-100">
          <div className="d-flex h-100 text-end align-items-end">
            <div className="me-auto p-2">
              <div id="mute">
                <svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" viewBox="-12 -7 45 40" fill="white" stroke="white" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" className="feather feather-volume-x"><polygon points="11 5 6 9 2 9 2 15 6 15 11 19 11 5"></polygon><line x1="23" y1="9" x2="17" y2="15"></line><line x1="17" y1="9" x2="23" y2="15"></line></svg>
              </div>
            </div>
            <div className="overlay">
              <div className="text-white text-center">
                <h4>Heading</h4>
                <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Et facere ratione enim cupiditate provident eligendi sunt tenetur nihil ad atque.</p>
              </div>
            </div>
          </div>
        </div>
      </header>


      <section id="about" style={{ backgroundColor: "#f05f40" }}>
        <div className="container">
          <div className="row">
            <div className="col offset-lg-8 mx-auto text-center">
              <h2 className="text-white section-heading">We've got what you need!</h2>
              <hr className="light my-4" />
              <p className="text-faded mb-4" style={{ fontSize: "17px" }}><strong>FundingX&nbsp;</strong>has everything you need to get your new campaign up and running in no time! All projects are verified and tested, feel free to back them. No strings attached! We follow the All or Nothing policy.&nbsp;<br />We support <strong>Made in Bha</strong>r<strong>at!&nbsp;</strong><br /></p><a className="btn btn-light btn-xl js-scroll-trigger" role="button" href="./ref/about us.html">What we do</a>
            </div>
          </div>
        </div>
      </section>

      <div className="hpp">
        <pre>
        </pre>
        <div className="secondbox">
          <div className="container">
            <div className="row">
              <div className="col-lg-12 text-center">
                <h2 className="section-heading">Handpicked Projects</h2>
                <hr className="my-4" style={{ color: "hotpink", height: "7px", margin: "auto" }} />
              </div>
            </div>
          </div>
          <div className="container">
            <div className="row">
              <div className="col-xs-12 col-md-4 col-lg-3">
                <div className="card" style={{ borderRadius: "3%", margin: "10px 10px 20px" }}>
                  <img className="card-img-top myimg" src="https://electrobotic.in/wp-content/uploads/2020/07/Black1.2-768x768.png" />
                  <div className="container text-center">
                    <h4 className="card-title" style={{ height: "55px" }}><br />Hasten 720 Black<br /></h4>
                    <h6 className="text-muted card-subtitle mb-2">Dual camera Drone Hasten720’s stability centric design makes it perfect for flying indoors and outdoor for an exceptional flying experience.<br /><br /><br /></h6>
                    <div className="progress" style={{ marginBottom: "10px" }}>
                      <div className="progress-bar bg-success" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100" role="progressbar" style={{ width: "25%" }}>27%</div>
                    </div>
                    <p className="card-text">₹402,546<br />by&nbsp;20 backers<br /></p><a className="card-link " href="./prod/hasten/product.html" style={{ color: "blue" }}>Know&nbsp; More<br /></a>
                  </div>

                </div>
              </div>
              <div className="col-xs-12 col-md-4 col-lg-3">
                <div className="card" style={{ bordeRadius: "3%", margin: "10px 10px 20px" }}>
                  <img className="card-img-top myimg" src="https://electrobotic.in/wp-content/uploads/2020/07/Black1.2-768x768.png" />
                  <div className="container text-center">
                    <h4 className="card-title" style={{ height: "55px" }}><br />Hasten 720 Black<br /></h4>
                    <h6 className="text-muted card-subtitle mb-2">Dual camera Drone Hasten720’s stability centric design makes it perfect for flying indoors and outdoor for an exceptional flying experience.<br /><br /><br /></h6>
                    <div className="progress" style="margin-bottom: 10px;">
                      <div className="progress-bar bg-success" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100" role="progressbar" style={{ width: "25%" }}>27%</div>
                    </div>
                    <p className="card-text">₹402,546<br />by&nbsp;20 backers<br /></p><a className="card-link " href="./prod/hasten/product.html" style={{ color: "blue" }}>Know&nbsp; More<br /></a>
                  </div>
                </div>
              </div>
              <div className="col-xs-12 col-md-4 col-lg-3">
                <div className="card" style="border-radius: 3%; margin: 10px 10px 20px;">
                  <img className="card-img-top myimg" src="https://electrobotic.in/wp-content/uploads/2020/07/Black1.2-768x768.png" />
                  <div className="container text-center">
                    <h4 className="card-title" style={{ height: "55px" }}><br />Hasten 720 Black<br /></h4>
                    <h6 className="text-muted card-subtitle mb-2">Dual camera Drone Hasten720’s stability centric design makes it perfect for flying indoors and outdoor for an exceptional flying experience.<br /><br /><br /></h6>
                    <div className="progress" style={{ marginBottom: "10px" }}>
                      <div className="progress-bar bg-success" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100" role="progressbar" style={{ width: "25%" }}>27%</div>
                    </div>
                    <p className="card-text">₹402,546<br />by&nbsp;20 backers<br /></p><a className="card-link " href="./prod/hasten/product.html" style={{ color: "blue" }}>Know&nbsp; More<br /></a>
                  </div>

                </div>
              </div>
              <div className="col-xs-12 col-md-4 col-lg-3">
                <div className="card" style={{ borderRadius: "3%", margin: "10px 10px 20px" }}>
                  <img className="card-img-top myimg" src="https://electrobotic.in/wp-content/uploads/2020/07/Black1.2-768x768.png" />
                  <div className="container text-center">
                    <h4 className="card-title" style={{ height: "55px" }}><br />Hasten 720 Black<br /></h4>
                    <h6 className="text-muted card-subtitle mb-2">Dual camera Drone Hasten720’s stability centric design makes it perfect for flying indoors and outdoor for an exceptional flying experience.<br /><br /><br /></h6>
                    <div className="progress" style={{ marginBottom: "10px" }}>
                      <div className="progress-bar bg-success" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100" role="progressbar" style={{ width: "25%" }}>27%</div>
                    </div>
                    <p className="card-text">₹402,546<br />by&nbsp;20 backers<br /></p><a className="card-link " href="./prod/hasten/product.html" style={{ color: "blue" }}>Know&nbsp; More<br /></a>
                  </div>

                </div>
              </div>
            </div>
          </div>
        </div>


        <Portfolio/>

        <section className="page-section" id="about">
          <div className="container">
            <div className="text-center">
              <h2 className="section-heading text-uppercase">About</h2>
              <h3 className="section-subheading text-muted">Lorem ipsum dolor sit amet consectetur.</h3>
            </div>
            <ul className="timeline">
              <li>
                <div className="timeline-image"><img className="rounded-circle img-fluid" src="assets/img/about/1.jpg" alt="" /></div>
                <div className="timeline-panel">
                  <div className="timeline-heading">
                    <h4>2009-2011</h4>
                    <h4 className="subheading">Our Humble Beginnings</h4>
                  </div>
                  <div className="timeline-body"><p className="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt ut voluptatum eius sapiente, totam reiciendis temporibus qui quibusdam, recusandae sit vero unde, sed, incidunt et ea quo dolore laudantium consectetur!</p></div>
                </div>
              </li>
              <li className="timeline-inverted">
                <div className="timeline-image"><img className="rounded-circle img-fluid" src="assets/img/about/2.jpg" alt="" /></div>
                <div className="timeline-panel">
                  <div className="timeline-heading">
                    <h4>March 2011</h4>
                    <h4 className="subheading">An Agency is Born</h4>
                  </div>
                  <div className="timeline-body"><p className="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt ut voluptatum eius sapiente, totam reiciendis temporibus qui quibusdam, recusandae sit vero unde, sed, incidunt et ea quo dolore laudantium consectetur!</p></div>
                </div>
              </li>
              <li>
                <div className="timeline-image"><img className="rounded-circle img-fluid" src="assets/img/about/3.jpg" alt="" /></div>
                <div className="timeline-panel">
                  <div className="timeline-heading">
                    <h4>December 2012</h4>
                    <h4 className="subheading">Transition to Full Service</h4>
                  </div>
                  <div className="timeline-body"><p className="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt ut voluptatum eius sapiente, totam reiciendis temporibus qui quibusdam, recusandae sit vero unde, sed, incidunt et ea quo dolore laudantium consectetur!</p></div>
                </div>
              </li>
              <li className="timeline-inverted">
                <div className="timeline-image"><img className="rounded-circle img-fluid" src="assets/img/about/4.jpg" alt="" /></div>
                <div className="timeline-panel">
                  <div className="timeline-heading">
                    <h4>July 2014</h4>
                    <h4 className="subheading">Phase Two Expansion</h4>
                  </div>
                  <div className="timeline-body"><p className="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt ut voluptatum eius sapiente, totam reiciendis temporibus qui quibusdam, recusandae sit vero unde, sed, incidunt et ea quo dolore laudantium consectetur!</p></div>
                </div>
              </li>
              <li className="timeline-inverted">
                <div className="timeline-image">
                  <h4>
                    Be Part
                                <br />
                                Of Our
                                <br />
                                Story!
                            </h4>
                </div>
              </li>
            </ul>
          </div>
        </section>

        <section className="bg-dark text-white">
          <div className="container text-center">
            <h2 className="mb-4">Join our Groups to get our latest product info</h2>
          </div>
          <div className="row">
            <div className="col-lg-6 col-sm-12" style={{ textAlign: "center" }}><h3><i className="fa fa-whatsapp" style={{ fontSize: "36px", color: "green" }}></i>   Whatsapp</h3></div>
            <div className="col-lg-6 col-sm-12" style={{ textAlign: "center" }}><h3><i className="fa fa-telegram" style={{ fontSize: "36px", color: "skyblue" }}></i>   Telegram</h3></div>
          </div>
          <div className="row">
            <div className="col-lg-6 col-sm-12" style={{ textAlign: "center" }}><h5><a href="https://chat.whatsapp.com/INYndaPsNv04OUjzn4qqC7">https://chat.whatsapp.com/INYndaPsNv04OUjzn4qqC7</a></h5></div>
            <div className="col-lg-6 col-sm-12" style={{ textAlign: "center" }}><h5><a href="https://t.me/fundingx">https://t.me/fundingx</a></h5></div>
          </div>
        </section>




        <footer className="Footer_bottom">
          <div className="container">

            <div className="row">
              <div className="col-lg-6 col-sm-12">
                <h2 className="Footer_row1_col1_heading">Sign Up To Our Newsletter.</h2>
                <h4 className="Footer_row1_col1_text">Be The First To Hear About The</h4>
                <h4 className="Footer_row1_col1_text">Latest Offers.</h4>
              </div>
              <div className="col-lg-3 col-sm-6">
                <input type="text" name="#" placeholder="Your Email" className="Footer_email_newsletter" />
              </div>
              <div className="col-lg-3 col-sm-6">
                <button type="button" name="Footer_button_Subscribe" className="Footer_row1_col3_button btn-primary"><span className="Footer_row1_col3_button_text">Subscribe</span></button>
              </div>
            </div>
          </div>

          <div className="container">
            <div className="row">
              <div className="col-lg-3 col-sm-6 Footer_row2_col1">
                <h3 className="Footer_row2_col1_heading">EXPLORE</h3>
                <h4 className="Footer_row2_col1_content"><a href="#">What We Do</a></h4>
                <h4 className="Footer_row2_col1_content"><a href="#">Funding</a></h4>
              </div>
              <div className="col-lg-3 col-sm-6">
                <h3 className="Footer_row2_col1_heading">ABOUT</h3>
                <h4 className="Footer_row2_col1_content"><a href="./ref/about us.html">About Us</a></h4>
                <h4 className="Footer_row2_col1_content"><a href="#">Blog</a></h4>
                <h4 className="Footer_row2_col1_content"><a href="#">Trust & Safety</a></h4>
                <h4 className="Footer_row2_col1_content"><a href="#">Help & Support</a></h4>
                <h4 className="Footer_row2_col1_content"><a href="#">Press</a></h4>
                <h4 className="Footer_row2_col1_content"><a href="#">Careers</a></h4>
                <h4 className="Footer_row2_col1_content"><a href="#">Contact</a></h4>
              </div>
              <div className="col-lg-3 col-sm-6">
                <h3 className="Footer_row2_col1_heading">Entrepreneurs</h3>
                <h4 className="Footer_row2_col1_content"><a href="#">How It Works</a></h4>
                <h4 className="Footer_row2_col1_content"><a href="#">Kickstarter vs FundingX</a></h4>
                <h4 className="Footer_row2_col1_content"><a href="#">Education Centre</a></h4>
                <h4 className="Footer_row2_col1_content"><a href="#">Experts Directory</a></h4>
                <h4 className="Footer_row2_col1_content"><a href="#">Fees</a></h4>
                <h4 className="Footer_row2_col1_content"><a href="#">Enterperise</a></h4>
              </div>
              <div className="col-lg-3 col-sm-6">
                <h3 className="Footer_row2_col1_heading">Contact Us</h3>
                <h4 className="Footer_row2_col1_content">Phone: <a href="+917982423491">+917982423491</a></h4>
                <h4 className="Footer_row2_col1_content">Email: <a href="connec@fundingx.in">connect@fundingx.in</a></h4>
              </div>
            </div>

            <div className="row Footer_row3">

              <div className="col-lg-2 col-sm-0">

              </div>
              <div className="col-lg-2 col-sm-2">
                <h6 className="Footer_row3_content"><a href="#">Term Of Use</a></h6>
              </div>
              <div className="col-lg-2 col-sm-2">
                <h6 className="Footer_row3_content"><a href="#">Privacy Policy</a></h6>
              </div>
              <div className="col-lg-2 col-sm-2">
                <h6 className="Footer_row3_content"><a href="#">Cookie Policy</a></h6>
              </div>
              <div className="col-lg-2 col-sm-2">
                <h6 className="Footer_row3_content"><a href="#">Do Not Sell My Personal Information</a></h6>
              </div>
              <div className="col-lg-2 col-sm-2">
                <h6 className="Footer_row3_content"><a href="#"><i className="fa fa-copyright"> 2021 FundingX All Right Reserved</i> </a></h6>
              </div>
            </div>

            <div className="row Footer_row4">
              <div className="col-lg-12 col-sm-12">
              </div>
            </div>

            <div className="row Footer_row5">
              <div className="col-lg-4 col-sm-4">

                <div className="row">
                  <div className="col-lg-1 col-sm-1 Footer_row5_content"><a href="#"><i className="fa fa-facebook-square"></i></a></div>
                  <div className="col-lg-1 col-sm-1 Footer_row5_content"><a href="#"><i className="fa fa-instagram"></i></a></div>
                </div>
              </div>
              <div className="col-lg-4 col-sm-4 Footer_row5_content">

              </div>
              <div className="col-lg-4 col-sm-4 Footer_row5_content">
                Copyright <i className="fa fa-copyright"> </i> 2021 FundingX
              </div>
            </div>
          </div>
        </footer>
      </div>
    </div>
